//滾動animation
    //把控制項加入陣列       
    var fadein_index=[
            ".aboutus h1",
            ".aboutus .container",
            ".service h1",
            ".service p",
            ".service .container",
            ".section_news .col-sm-3",
            ".section_news .col-sm-9"
            ];

    if ($(window).width() > 767) {
        //當視窗高度到達物件時的動作
        function scrollanimation(a,b){
                $(a).each(function() {
              // Check the location of each desired element //
              var objectBottom = $(this).offset().top + $(this).outerHeight();
              var windowBottom = $(window).scrollTop() + $(window).innerHeight();
              if (objectBottom < windowBottom) {
                    $(this).addClass(b);
                }
            });     
        };

        //遇到class+animation
        $(window).on("load",function() {
          $(window).scroll(function() {     
              //利用forEach去產生相對數量的function
              fadein_index.forEach(function(e){
                scrollanimation(e,'animated fadeIn');
              })
          }); 
        }); 
    }